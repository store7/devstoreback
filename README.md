# REST API Store

This is a rest api to administrate a store.

## End-points

api/products/

methods: ```GET | POST | DELETE | PUT```

___
api/companies/

methods: ```GET | POST | DELETE | PUT```
___

api/users/

methods: ```GET | POST | DELETE | PUT```
___

[Store Api documentation](https://documenter.getpostman.com/view/1695109/SzmYA2GF?version=latest)

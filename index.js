const express = require('express');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const productsApi = require('./src/routes/products');
const usersApi = require('./src/routes//users');
const companiesApi = require('./src/routes/companies');
const authApp = require('./src/routes/auth');

const { config } = require('./src/config');
const { logError, errorHandler, wrapError } = require('./src/utils/middleware/errorHandler');
const notFoundHandler = require('./src/utils/middleware/notFoundHandler');
const cors = require('cors');
const app = express();


const corsOptions = {
    origin: config.app.corsOrigin
}

//Cors configuration
app.use(cors(corsOptions));


//Body parser
app.use(bodyParser.json());
app.use(helmet());
//Routes
productsApi(app);
companiesApi(app);
usersApi(app);
authApp(app);

//Error handler
app.use(logError);
app.use(wrapError);
app.use(errorHandler);

//Not found handler
app.use(notFoundHandler);

app.listen( config.app.port, () => {
    console.log(`The server is listening in the port: ${config.app.port}`);  
})
require('dotenv').config();

const config = {
    dev: process.env.MODE_ENV !== 'produccion',
    app: {
        port: process.env.PORT || 3000,
        corsOrigin: process.env.APP_ORIGIN_CORS
    },
    db: {
        port: process.env.PORT,
        dbUser: process.env.DB_USER,
        dbPassword: process.env.DB_PASSWORD,
        dbHost: process.env.DB_HOST,
        dbName: process.env.DB_NAME,
    },
    user: {
        defaultAdminPassword: process.env.DEFAULT_ADMIN_PASSWORD,
        defaultUserPassword: process.env.DEFAULT_USER_PASSWORD
    },
    auth: {
        authJwtSecret: process.env.AUTH_JWT_SECRET
    },
    keys: {
        publicApiKeyToken: process.env.PUBLIC_API_KEY_TOKEN,
        adminApiKeyToken: process.env.ADMIN_API_KEY_TOKEN
    }

}

module.exports = {
    config
}
const assert = require('assert');
const boom = require('@hapi/boom');
const proxyquire = require('proxyquire');
const jwt = require('jsonwebtoken');
const testServer = require('./../utils/testServer');

describe('Routes - sign-in', function(){
    const route = proxyquire('./../routes/auth',{});

    const request = testServer(route);

    describe('Post api/auth/sign-in/', function(){
        it('should response with status 401', function(done){
            request.post(`/api/auth/sign-in`)
            .auth('test@test.com', 't35tP45m0rd')
            .expect(401, done()).to.throw(new Error('api key token is required'));
        });
    })
})
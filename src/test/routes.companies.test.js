const assert = require('assert');
const proxyquire = require('proxyquire');
const jwt = require('jsonwebtoken');
const { companies, CompanyServiceMock, companyToCreate } = require('./../mocks/companyMock');
const idCompany = '5eac28047cdd9b1ac0621da3';

const testServer = require('./../utils/testServer');
const { config } = require('./../config/index');

function generateToken(){
    const payload = { 
        sub: '5eac65408620d23930c491c8', 
        username: 'testuser', 
        email: 'test@test.com',
        scopes: ['read:companies', 'create:companies', 'update:companies', 'delete:companies']
    };

    return jwt.sign(payload, config.auth.authJwtSecret, { expiresIn: '15m' });
}

describe('Routes - companies', function(){
    const route = proxyquire('./../routes/companies', {
        './../services/company': CompanyServiceMock
    });

    const request = testServer(route);
    const token = generateToken();

    
    // Get method test
    describe('Get /companies/', function(){
        it('should response with status 200', function(done){
            request.get('/api/companies')
            .set('Authorization', `bearer ${token}`)
            .expect(200, done);
        })

        
        it('should response with list of companies', function(done){
            request.get('/api/companies')
            .set('Authorization', `bearer ${token}`)
            .end((err, res) => {
                assert.deepEqual(res.body, {
                    data: companies,
                    message: 'Companies listed'
                }, done());
            });
        })

        it('filter should response with status 200', function(done){
            request.get(`/api/companies/${idCompany}`)
            .set('Authorization', `bearer ${token}`)
            .expect(200, done);
        })

        
        it('filter should response with company', function(done){
            const company = companies.find(company => company.id === idCompany);

            request.get(`/api/companies/${idCompany}`)
            .set('Authorization', `bearer ${token}`)
            .end((err, res) => {
                assert.deepEqual(res.body, {
                    data: company,
                    message: 'Company retrieved'
                });
                done();
            });
        })
    });

    // Delete method test
    describe('Delete /companies/:id', function(){
        it('should response with status 200', function(done){
            request.delete(`/api/companies/${idCompany}`)
            .set('Authorization', `bearer ${token}`)
            .expect(200, done);
        })

        it('should response with id company deleted', function(done){
            request.delete(`/api/companies/${idCompany}`)
            .set('Authorization', `bearer ${token}`)
            .end((err, res) => {
                assert.deepEqual(res.body, {
                    data: idCompany,
                    message: 'Company deleted'
                });
                done();
            });
        });
    });

    // Post method test
    describe('Post /companies/', function(){   
        it('should response with status 201', function(done){
    
            request.post(`/api/companies/`)
            .send(companyToCreate)
            .set('Accept', 'application/json')
            .set('Authorization', `bearer ${token}`)
            .expect(201, done);
        });
        
        it('should response with status 201 and id company created', function(done){
    
            request.post(`/api/companies/`)
            .send(companyToCreate)
            .set('Accept', 'application/json')
            .set('Authorization', `bearer ${token}`)
            .end((err, res) => {
                assert.deepEqual(res.body, {
                    data: idCompany,
                    message: 'Company created'
                });
            },done());
        });
    });

    // Put method test
    describe('Put /companies/', function(){   
        it('should response with status 200', function(done){
    
            request.put(`/api/companies/${idCompany}`)
            .send(companyToCreate)
            .set('Accept', 'application/json')
            .set('Authorization', `bearer ${token}`)
            .expect(200, done);
        });

        it('should response with status 200 and id company updated', function(done){
    
            request.put(`/api/companies/${idCompany}`)
            .send(companyToCreate)
            .set('Accept', 'application/json')
            .set('Authorization', `bearer ${token}`)
            .end((err, res) => {
                assert.deepEqual(res.body, {
                    data: idCompany,
                    message: 'Company updated'
                });
            },done());
        });
    });
})
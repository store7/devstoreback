const assert = require('assert');
const proxyquire = require('proxyquire');
const jwt = require('jsonwebtoken');
const { UserServiceMock, userToCreate, users } = require('./../mocks/userMock');
const idUser = '5eac2510b073fa0dac0e3568';

const testServer = require('./../utils/testServer');
const { config } = require('./../config/index');

function generateToken(){
    const payload = { 
        sub: '5eac65408620d23930c491c8', 
        username: 'testuser', 
        email: 'test@test.com',
        scopes: ['read:users', 'create:users', 'update:users', 'delete:users']
    };

    return jwt.sign(payload, config.auth.authJwtSecret, { expiresIn: '15m' });
}

describe('Routes - users', function(){
    const route = proxyquire('./../routes/users', {
        './../services/user': UserServiceMock
    });

    const request = testServer(route);
    const token = generateToken();

    
    // Get method test
    describe('Get /users/', function(){
        it('should response with status 200', function(done){
            request.get('/api/users')
            .set('Authorization', `bearer ${token}`)
            .expect(200, done);
        })

        
        it('should response with list of users', function(done){
            request.get('/api/users')
            .set('Authorization', `bearer ${token}`)
            .end((err, res) => {
                assert.deepEqual(res.body, {
                    data: users,
                    message: 'Users listed'
                }, done());
            });
        })

        it('filter should response with status 200', function(done){
            request.get(`/api/users/${idUser}`)
            .set('Authorization', `bearer ${token}`)
            .expect(200, done);
        })

        
        it('filter should response with user', function(done){
            const user = users.find(user => user.id === idUser);

            request.get(`/api/users/${idUser}`)
            .set('Authorization', `bearer ${token}`)
            .end((err, res) => {
                assert.deepEqual(res.body, {
                    data: user,
                    message: 'User retrieved'
                });
                done();
            });
        })
    });

    // Delete method test
    describe('Delete /users/:id', function(){
        it('should response with status 200', function(done){
            request.delete(`/api/users/${idUser}`)
            .set('Authorization', `bearer ${token}`)
            .expect(200, done);
        })

        it('should response with id user deleted', function(done){
            request.delete(`/api/users/${idUser}`)
            .set('Authorization', `bearer ${token}`)
            .end((err, res) => {
                assert.deepEqual(res.body, {
                    data: idUser,
                    message: 'User deleted'
                });
                done();
            });
        });
    });

    // Post method test
    describe('Post /users/', function(){   
        it('should response with status 201', function(done){
    
            request.post(`/api/users/`)
            .send(userToCreate)
            .set('Accept', 'application/json')
            .set('Authorization', `bearer ${token}`)
            .expect(201, done);
        });
        
        it('should response with status 201 and id user created', function(done){
    
            request.post(`/api/users/`)
            .send(userToCreate)
            .set('Accept', 'application/json')
            .set('Authorization', `bearer ${token}`)
            .end((err, res) => {
                assert.deepEqual(res.body, {
                    data: idUser,
                    message: 'User created'
                });
            },done());
        });
    });

    // Put method test
    describe('Put /users/', function(){   
        it('should response with status 200', function(done){
    
            request.put(`/api/users/${idUser}`)
            .send(userToCreate)
            .set('Accept', 'application/json')
            .set('Authorization', `bearer ${token}`)
            .expect(200, done);
        });

        it('should response with status 200 and id user updated', function(done){
    
            request.put(`/api/users/${idUser}`)
            .send(userToCreate)
            .set('Accept', 'application/json')
            .set('Authorization', `bearer ${token}`)
            .end((err, res) => {
                assert.deepEqual(res.body, {
                    data: idUser,
                    message: 'User updated'
                });
            },done());
        });
    });
})
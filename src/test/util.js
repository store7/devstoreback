const jwt = require('jsonwebtoken');
const { config } = require('./../config/index');

function generateToken(){
    const payload = { 
        sub: '5eac65408620d23930c491c8', 
        username: 'userTest', 
        email: 'usertest@test.com',
        scopes: ['read:products', 'create:products', 'update:products', 'delete:products']
    };

    return jwt.sign(payload, config.auth.authJwtSecret, { expiresIn: '15m' });
}

module.exports = {
    generateToken
}
const assert = require('assert');
const axios = require('axios');
const proxyquire = require('proxyquire');
const jwt = require('jsonwebtoken');
const {producs, filterMovieMock, ProductsServiceMock, productToCreate } = require('./../mocks/produtsMock');
const idProduct = '5eaac0e7e5b4af1a4cd56210';

const testServer = require('./../utils/testServer');
const { config } = require('./../config/index');

function generateToken(){
    const payload = { 
        sub: '5eac65408620d23930c491c8', 
        username: 'testuser', 
        email: 'test@test.com',
        scopes: ['read:products', 'create:products', 'update:products', 'delete:products']
    };

    return jwt.sign(payload, config.auth.authJwtSecret, { expiresIn: '15m' });
}

describe('Routes - products', function(){
    const route = proxyquire('./../routes/products', {
        './../services/products': ProductsServiceMock
    });

    const request = testServer(route);
    const token = generateToken();

    
    // Get method test
    describe('Get /products/', function(){
        it('should response with status 200', function(done){
            request.get('/api/products')
            .set('Authorization', `bearer ${token}`)
            .expect(200, done);
        })

        it('should response with list of products', function(done){
            request.get('/api/products')
            .set('Authorization', `bearer ${token}`)
            .end((err, res) => {
                assert.deepEqual(res.body, {
                    data: producs,
                    message: 'Products listed'
                }, done());
            });
        })

        it('filter should response with status 200', function(done){
            request.get(`/api/products/${idProduct}`)
            .set('Authorization', `bearer ${token}`)
            .expect(200, done);
        })

        
        it('filter should response with product', function(done){
            const produc = producs.find(produc => produc.id === idProduct);

            request.get(`/api/products/${idProduct}`)
            .set('Authorization', `bearer ${token}`)
            .end((err, res) => {
                assert.deepEqual(res.body, {
                    data: produc,
                    message: 'Product retrieved'
                });
                done();
            });
        })
    });

    // Delete method test
    describe('Delete /products/:id', function(){
        it('should response with status 200', function(done){
            request.delete(`/api/products/${idProduct}`)
            .set('Authorization', `bearer ${token}`)
            .expect(200, done);
        })

        it('should response with id product deleted', function(done){
            request.delete(`/api/products/${idProduct}`)
            .set('Authorization', `bearer ${token}`)
            .end((err, res) => {
                assert.deepEqual(res.body, {
                    data: idProduct,
                    message: 'Product deleted'
                });
                done();
            });
        });
    });


    // Post method test
    describe('Post /products/', function(){   
        it('should response with status 201', function(done){
    
            request.post(`/api/products/`)
            .send(productToCreate)
            .set('Accept', 'application/json')
            .set('Authorization', `bearer ${token}`)
            .expect(201, done);
        });
        
        it('should response with status 201 and id product created', function(done){
    
            request.post(`/api/products/`)
            .send(productToCreate)
            .set('Accept', 'application/json')
            .set('Authorization', `bearer ${token}`)
            .end((err, res) => {
                assert.deepEqual(res.body, {
                    data: idProduct,
                    message: 'Product created'
                });
            },done());
        });
    });


    // Put method test
    describe('Put /products/', function(){   
        it('should response with status 200', function(done){
    
            request.put(`/api/products/${idProduct}`)
            .send(productToCreate)
            .set('Accept', 'application/json')
            .set('Authorization', `bearer ${token}`)
            .expect(200, done);
        });

        it('should response with status 200 and id product updated', function(done){
    
            request.put(`/api/products/${idProduct}`)
            .send(productToCreate)
            .set('Accept', 'application/json')
            .set('Authorization', `bearer ${token}`)
            .end((err, res) => {
                assert.deepEqual(res.body, {
                    data: idProduct,
                    message: 'Product updated'
                });
            },done());
        });
    });    
})
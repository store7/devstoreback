const MongoLib = require('./../lib/index');

class CompanyService {
    constructor(){
        this.collection = 'companies';
        this.mongoDb = new MongoLib();
    }

    async getCompanies(){
        const listCompany = await this.mongoDb.getAll(this.collection, {});
        return listCompany || [];
    }

    async getCompany(id){
        const company = await this.mongoDb.get(this.collection, id);
        return company || {};
    }

    async createCompany({company}){
        const createCompanyId = this.mongoDb.create(this.collection, company);
        return createCompanyId;
    }

    async updateCompany({id, company}){
        const updateCompanyId = await this.mongoDb.update(this.collection, id, company);
        return updateCompanyId;
    }

    async deleteCompany({id }){
        const deleteCompanyId = await this.mongoDb.delete(this.collection, id);
        return deleteCompanyId;
    }
}

module.exports = CompanyService;
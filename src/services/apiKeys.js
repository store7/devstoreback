const MongoLib = require('./../lib/index');

class ApiKeyService {
    constructor(){
        this.collection = 'api-keys';
        this.mongoDb = new MongoLib();
    }

    async getApiKey({ token }){
        const [ apiKey ] = await this.mongoDb.getAll(this.collection, { token });
        return apiKey;
    }
}

module.exports = ApiKeyService;
const MongoLib = require('./../lib/index');
const bcrypt = require('bcrypt');
class UserService {
    constructor(){
        this.collection = 'users';
        this.mongoDb = new MongoLib();
    }

    async getUsers() {
        const listUsers = await this.mongoDb.getAll(this.collection, {});
        return listUsers || [];
    }

    async getUser({ email }) {
        const [user] = await this.mongoDb.getAll(this.collection, { email });
        return user || {};
    }

    async createUser({user}) {
        const {password} = user;
        user.password = await bcrypt.hash(password, 10);
        
        const createUserId = this.mongoDb.create(this.collection, user);
        return createUserId;
    }

    async updateUser({id, user}) {
        const updateUserId = await this.mongoDb.update(this.collection, id, user);
        return updateUserId;
    }

    async deleteUser({ id }) {
        const deleteUserId = await this.mongoDb.delete(this.collection, id);
        return deleteUserId;
    }
}

module.exports = UserService;
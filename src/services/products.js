const MongoLib = require('./../lib/index');

class ProductsService {
    constructor(){
        this.collection = 'products';
        this.mongoDb = new MongoLib();
    }
    
    async getProducts() {
        const listProducts = await this.mongoDb.getAll(this.collection, {});
        return listProducts || [];
    }

    async getProduct(id) {
        const product = await this.mongoDb.get(this.collection, id);
        return product || {};
    }

    async createProduct({ product }) {
        const createProductId = this.mongoDb.create(this.collection, product);
        return createProductId;
    }

    async updateProduct({id,  product} = {} ) {
        const updateProductId = await this.mongoDb.update(this.collection, id, product);
        return updateProductId;
    }

    async deleteProduct({ id }) {
        const deleteProductId = await this.mongoDb.delete(this.collection, id);
        return deleteProductId;
    }
}

module.exports = ProductsService
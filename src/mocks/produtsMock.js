const producs = 
[
    {"id":"5eaac0e7e5b4af1a4cd56210","name":"Sherbet - Raspberry","image":"http://dummyimage.com/178x104.jpg/cc0000/ffffff","description":"error: undefined method `/' for nil:NilClass","price":77.3, "company": "5eaac0e7e5b4af1a4cd56210" },
    {"id":"5eaac0e7e5b4af1a4cd56211","name":"Dried Peach","image":"http://dummyimage.com/241x143.bmp/ff4444/ffffff","description":"error: undefined method `/' for nil:NilClass","price":11.64, "company": "5eaac0e7e5b4af1a4cd56210"},
    {"id":"5eaac0e7e5b4af1a4cd56212","name":"Compound - Passion Fruit","image":"http://dummyimage.com/118x228.bmp/5fa2dd/ffffff","description":"error: undefined method `/' for nil:NilClass","price":19.36, "company": "5eaac0e7e5b4af1a4cd56210"},
    {"id":"5eaac0e7e5b4af1a4cd56213","name":"Lobster - Baby, Boiled","image":"http://dummyimage.com/131x152.png/cc0000/ffffff","description":"error: undefined method `/' for nil:NilClass","price":31.03, "company": "5eaac0e7e5b4af1a4cd56210"},
    {"id":"5eaac0e7e5b4af1a4cd56214","name":"Shrimp - Baby, Warm Water","image":"http://dummyimage.com/130x117.png/cc0000/ffffff","description":"error: undefined method `/' for nil:NilClass","price":8.07, "company": "5eaac0e7e5b4af1a4cd56210"},
    {"id":"5eaac0e7e5b4af1a4cd56215","name":"Coffee - Beans, Whole","image":"http://dummyimage.com/106x233.bmp/dddddd/000000","description":"error: undefined method `/' for nil:NilClass","price":54.72, "company": "5eaac0e7e5b4af1a4cd56210"},
    {"id":"5eaac0e7e5b4af1a4cd56216","name":"Appetizer - Mini Egg Roll, Shrimp","image":"http://dummyimage.com/113x194.png/dddddd/000000","description":"error: undefined method `/' for nil:NilClass","price":13.46, "company": "5eaac0e7e5b4af1a4cd56210"},
    {"id":"5eaac0e7e5b4af1a4cd56217","name":"Nescafe - Frothy French Vanilla","image":"http://dummyimage.com/104x184.bmp/5fa2dd/ffffff","description":"error: undefined method `/' for nil:NilClass","price":82.59, "company": "5eaac0e7e5b4af1a4cd56210"},
    {"id":"5eaac0e7e5b4af1a4cd56218","name":"Bagel - Ched Chs Presliced","image":"http://dummyimage.com/115x182.bmp/dddddd/000000","description":"error: undefined method `/' for nil:NilClass","price":58.06, "company": "5eaac0e7e5b4af1a4cd56210"},
    {"id":"5eaac0e7e5b4af1a4cd56219","name":"Relish","image":"http://dummyimage.com/109x234.bmp/ff4444/ffffff","description":"error: undefined method `/' for nil:NilClass","price":10.45, "company": "5eaac0e7e5b4af1a4cd56210"}]

function filterMovieMock(id) {
    return producs.find(product => product.id === id);
}

class ProductsServiceMock {
    async getProducts(){
        return Promise.resolve(producs);
    }

    async getProduct(id){
        const product = producs.find(product => product.id === id);
        return Promise.resolve(producs[0]);
    }

    async deleteProduct({id}){
        return Promise.resolve(id);
    }

    async createProduct({ product }) {
        return Promise.resolve(producs[0].id);
    }

    async updateProduct({ id, product }) {
        return Promise.resolve(id);
    }
}

productToCreate = {
    "name":"Sherbet - Raspberry",
    "image":"http://dummyimage.com/178x104.jpg/cc0000/ffffff",
    "description":"error: undefined method `/' for nil:NilClass",
    "price":77.3, 
    "company": "5eaac0e7e5b4af1a4cd56210"
}

module.exports = {
    producs,
    filterMovieMock,
    ProductsServiceMock,
    productToCreate
}
const sinon = require('sinon');

const { producs, filterMovieMock } = require('../mocks/produtsMock');

const getAllStub = sinon.stub();

getAllStub.withArgs('products').resolves(producs);

const createStub = sinon.stub().resolves(producs[0].id);
const updateStub = sinon.stub().resolves(producs[0].id);
const deleteStub = sinon.stub().resolves(producs[0].id);

class MongoLibMock {
    getAll(collection, query){
        return getAllStub(collection, query);
    }

    create(collection, data){
        return createStub(collection, data);
    }

    update(collection, id, data){
        return updateStub(collection, id, data);
    }

    delete(collection, id){
        return deleteStub(collection, id);
    }
}

module.exports = {
    getAllStub,
    createStub,
    updateStub,
    deleteStub,
    MongoLibMock
}

const companies = [
    {
        "id": "5eac28047cdd9b1ac0621da3",
        "name": "CAMPOSB",
        "description": "Gallery of paints",
        "user": "5eac2510b073fa0dac0e3568"
    },
    {
        "id": "5eac281e7cdd9b1ac0621da4",
        "name": "CAMPOSB",
        "description": "Gallery of paints",
        "user": "5eac2510b073fa0dac0e3568",
        "image": "http://dummyimage.com/178x104.jpg/cc0000/ffffff"
    },
    {
        "id": "5eac9c049454c536b420c990",
        "name": "CAMPOSB",
        "description": "Gallery of paints",
        "user": "5eac2510b073fa0dac0e3568"
    }
];

class CompanyServiceMock {
    async getCompanies(){
        return Promise.resolve(companies);
    }

    async getCompany(id){
        const company = companies.find(company => company.id === id);
        return Promise.resolve(company);
    }

    async deleteCompany({id}){
        return Promise.resolve(id);
    }

    async createCompany({ company }) {
        return Promise.resolve(companies[0].id);
    }

    async updateCompany({ id, company }) {
        return Promise.resolve(id);
    }
}

companyToCreate = {
    "name": "CAMPOSB",
    "description": "Gallery of paints",
    "user": "5eac2510b073fa0dac0e3568"
}

module.exports = {
    companies,
    CompanyServiceMock,
    companyToCreate
}
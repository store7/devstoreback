const users = [
    {
        "id": "5eac2510b073fa0dac0e3568",
        "username": "CAMPOSB",
        "password": "CAMPOSB2019",
        "email": "jose.campos@ulfix.com"
    }
];

class UserServiceMock {
    async getUsers(){
        return Promise.resolve(users);
    }

    async getUser(id){
        const user = users.find(user => user.id === id);
        return Promise.resolve(user);
    }

    async deleteUser({id}){
        return Promise.resolve(id);
    }

    async createUser({ user }) {
        return Promise.resolve(users[0].id);
    }

    async updateUser({ id, user }) {
        return Promise.resolve(id);
    }
}

userToCreate = {
    "username": "CAMPOSB",
    "password": "CAMPOSB2019",
    "email": "jose.campos@ulfix.com"
}

module.exports = {
    UserServiceMock,
    userToCreate,
    users
}
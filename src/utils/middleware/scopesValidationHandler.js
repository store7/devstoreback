const boom = require('@hapi/boom');

function scopesValidationHandler(allowedScopes){
    return function(req, res, next) {
        if(!req.user || !(req.user || !req.user.scopes)){
            next(boom.unauthorized('Mising scopes'));
        }

        const hasAcces = allowedScopes.map(scope => req.user.scopes.includes(scope))
        .find(allowed => Boolean(allowed));

        if(hasAcces) {
            next();
        }else {
            next(boom.unauthorized('Insufficient scopes'));
        }
    }
}

module.exports = scopesValidationHandler;
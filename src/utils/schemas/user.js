const joi = require('@hapi/joi');

const userIdSchema = joi.string().regex(/^[0-9a-fA-F]{24}$/);
const userNameSchema = joi.string().max(50);
const userPhotoSchema = joi.string().uri();
const userPasswordSchema = joi.string();
const userEmailSchema = joi.string().email();

const userCreateSchema = {
    username: userNameSchema.required(),
    password: userPasswordSchema.required(),
	email: userEmailSchema.required(),
    photo: userPhotoSchema
}

const userUpdateSchema = {
    username: userNameSchema,
    password: userPasswordSchema,
	email: userEmailSchema,
    photo: userPhotoSchema
}

module.exports = {
    userCreateSchema,
    userUpdateSchema,
    userIdSchema
}
const joi = require('@hapi/joi');

const productIdSchema = joi.string().regex(/^[0-9a-fA-F]{24}$/);
const productNameSchema = joi.string().max(50);
const productImageSchema = joi.string().uri();
const productDescriptionSchema = joi.string().max(240);
const productPriceSchema = joi.number();
const productCompanySchema = joi.string().regex(/^[0-9a-fA-F]{24}$/);

const productCreateSchema = {
    name: productNameSchema.required(),
	image: productImageSchema.required(),
	description: productDescriptionSchema.required(),
	price: productPriceSchema.required(),
	company: productCompanySchema.required()
}

const prodctUpdateSchema = {
    name: productNameSchema,
	image: productImageSchema,
	description: productDescriptionSchema,
	price: productPriceSchema,
	company: productCompanySchema
}

module.exports = {
    productCreateSchema,
    prodctUpdateSchema,
    productIdSchema
}

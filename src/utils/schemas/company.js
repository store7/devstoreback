const joi = require('@hapi/joi');

const companyIdSchema = joi.string().regex(/^[0-9a-fA-F]{24}$/);
const companyNameSchema = joi.string().max(50);
const companyImageSchema = joi.string().uri();
const companyDescriptionSchema = joi.string().max(240);
const companyUserSchema = joi.string().regex(/^[0-9a-fA-F]{24}$/);

const companyCreateSchema = {
    name: companyNameSchema.required(),
	image: companyImageSchema,
	description: companyDescriptionSchema.required(),
	user: companyUserSchema.required()
}

const companyUpdateSchema = {
    name: companyNameSchema,
	image: companyImageSchema,
	description: companyDescriptionSchema,
	user: companyUserSchema
}

module.exports = {
    companyCreateSchema,
    companyUpdateSchema,
    companyIdSchema
}

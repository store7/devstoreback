const express = require('express');
const passport = require('passport');
const boom = require('@hapi/boom');
const jwt = require('jsonwebtoken');
const ApiKeyService = require('./../services/apiKeys');
const { config } = require('./../config/index');
//Basic strategy
require('./../utils/auth/strategies/basic');

function authApp(app){
    const router = express.Router();
    app.use('/api/auth', router);

    const apiKeyService = new ApiKeyService();

    router.post('/sign-in', async function(req, res, next){
        const { apiKeyToken } = req.body;

        if(!apiKeyToken){
            next(boom.unauthorized('api key token is required'));
        }

        passport.authenticate('basic', async function(error, user){
            try {
                
                if(error || !user){
                    next(boom.unauthorized());
                }

                req.login(user, { session: false }, async function (error) {
                    if(error){
                        next(error);
                    }
                });

                const apiKey = await apiKeyService.getApiKey({token: apiKeyToken});

                if(!apiKey){
                    next(boom.unauthorized());
                }

                const { _id: id, username, email } = user;
                const payload = {
                    sub: id,
                    username,
                    email,
                    scopes: apiKey.scopes
                }

                const token = jwt.sign(payload, config.auth.authJwtSecret, { expiresIn: '15m' });

                return res.status(200).json({ token, user: {id, username, email} })

            } catch (error) {
                next(error);
            }
        })(req, res, next);
    });
}

module.exports = authApp;
const express = require('express');
const passport = require('passport');

const CompanyService = require('./../services/company');
const { companyCreateSchema, companyUpdateSchema, companyIdSchema} = require('./../utils/schemas/company');
const validationHandler = require('./../utils/middleware/validationHandler');
const scopesValidationHandler = require('./../utils/middleware/scopesValidationHandler');

// Jwt strategy
require('./../utils/auth/strategies/jwt');


function companiesApi(app){
    const routeName = 'Company';
    const router = express.Router();
    const companyService = new CompanyService();

    app.use('/api/companies', router);

    router.get("/", 
    passport.authenticate('jwt', {session: false}), 
    scopesValidationHandler(['read:companies']),
    async function(req, res, next){
        try {

            const result = await  companyService.getCompanies();
            
            res.status(200).json({
                data: result,
                message: 'Companies listed'
            });
        } catch (error) {
            next(error);
        }
    });

    router.get("/:id", 
    passport.authenticate('jwt', {session: false}),
    scopesValidationHandler(['read:companies']),
    validationHandler({ id: companyIdSchema }, 'params'), 
    async function(req, res, next){
        try {
            const { id } = req.params;

            const result = await companyService.getCompany(id);
            
            res.status(200).json({
                data: result,
                message: `${routeName} retrieved`
            });
        } catch (error) {
            next(error);
        }
    });

    router.post("/", 
    passport.authenticate('jwt', {session: false}),
    scopesValidationHandler(['create:companies']),
    validationHandler(companyCreateSchema), 
    async function(req, res, next){
        try {
            const { body: company } = req;
            
            const result = await companyService.createCompany({company});
            
            res.status(201).json({
                data: result,
                message: `${routeName} created`
            });
        } catch (error) {
            next(error);
        }
    });

    router.put("/:id",
    passport.authenticate('jwt', {session: false}),
    scopesValidationHandler(['update:companies']),
    validationHandler({ id: companyIdSchema }, 'params'), 
    validationHandler(companyUpdateSchema), 
    async function(req, res, next){
        try {
            const { id } = req.params;
            const { body: company} = req;
            
            const result = await companyService.updateCompany({id, company});
            
            res.status(200).json({
                data: result,
                message: `${routeName} updated`
            });
        } catch (error) {
            next(error);
        }
    });

    router.delete("/:id",
    passport.authenticate('jwt', {session: false}),
    scopesValidationHandler(['delete:companies']),
    validationHandler({ id: companyIdSchema }, 'params'), 
    async function(req, res, next){
        try {
            const { id } = req.params;
            const result = await companyService.deleteCompany({id});
            
            res.status(200).json({
                data: result,
                message: `${routeName} deleted`
            });
        } catch (error) {
            next(error);
        }
    });
}

module.exports = companiesApi;
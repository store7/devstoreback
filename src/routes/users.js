const express = require('express');
const UserService = require('./../services/user');
const {userCreateSchema, userUpdateSchema, userIdSchema} = require('./../utils/schemas/user');
const validationHandler = require('./../utils/middleware/validationHandler');
const scopesValidationHandler = require('./../utils/middleware/scopesValidationHandler');
const passport = require('passport');

// Jwt strategy
require('./../utils/auth/strategies/jwt');

function usersApi(app){
    const routeName = 'User';
    const router = express.Router();
    const userService = new UserService();

    app.use('/api/users', router);
    
    router.get("/", 
    passport.authenticate('jwt', {session: false}),
    scopesValidationHandler(['read:users']),
    async function(req, res, next){
        try {

            const result = await userService.getUsers();
            
            res.status(200).json({
                data: result,
                message: 'Users listed'
            });
        } catch (error) {
            next(error);
        }
    });

    router.get("/:id", 
    passport.authenticate('jwt', {session: false}),
    scopesValidationHandler(['read:users']),
    validationHandler({ id: userIdSchema }, 'params'), 
    async function(req, res, next){
        try {
            const { id } = req.params;

            const result = await userService.getUser(id);
            
            res.status(200).json({
                data: result,
                message: `${routeName} retrieved`
            });
        } catch (error) {
            next(error);
        }
    });

    router.post("/", 
    passport.authenticate('jwt', {session: false}),
    scopesValidationHandler(['create:users']),
    validationHandler(userCreateSchema), 
    async function(req, res, next){
        try {
            const { body: user } = req;
            
            const result = await userService.createUser({user});
            
            res.status(201).json({
                data: result,
                message: `${routeName} created`
            });
        } catch (error) {
            next(error);
        }
    });

    router.put("/:id", 
    passport.authenticate('jwt', {session: false}),
    scopesValidationHandler(['update:users']),
    validationHandler({ id: userIdSchema }, 'params'), 
    validationHandler(userUpdateSchema), 
    async function(req, res, next){
        try {
            const { id } = req.params;
            const { body: user} = req;
            
            const result = await userService.updateUser({id, user});
            
            res.status(200).json({
                data: result,
                message: `${routeName} updated`
            });
        } catch (error) {
            next(error);
        }
    });

    router.delete("/:id", 
    passport.authenticate('jwt', {session: false}),
    scopesValidationHandler(['delete:users']),
    validationHandler({ id: userIdSchema }, 'params'), 
    async function(req, res, next){
        try {
            const { id } = req.params;
            const result = await userService.deleteUser({id});
            
            res.status(200).json({
                data: result,
                message: `${routeName} deleted`
            });
        } catch (error) {
            next(error);
        }
    });

}

module.exports = usersApi;
const express = require('express');
const ProductService = require('./../services/products');
const { productCreateSchema, prodctUpdateSchema, productIdSchema } = require('./../utils/schemas/product');
const validationHandler = require('./../utils/middleware/validationHandler');
const scopesValidationHandler = require('./../utils/middleware/scopesValidationHandler');
const passport = require('passport');

// Jwt strategy
require('./../utils/auth/strategies/jwt');

function productsApi(app){
    const routeName = 'Product';
    const router = express.Router();
    const productService = new ProductService();

    app.use('/api/products', router);

    router.get("/", 
    passport.authenticate('jwt', {session: false}),
    scopesValidationHandler(['read:products']),
    async function(req, res, next){
        try {

            const result = await  productService.getProducts();
            
            res.status(200).json({
                data: result,
                message: 'Products listed'
            });
        } catch (error) {
            next(error);
        }
    });

    router.get("/:id", 
    passport.authenticate('jwt', {session: false}),
    scopesValidationHandler(['read:products']),
    validationHandler({ id: productIdSchema }, 'params'),
     async function(req, res, next){
        try {
            const { id } = req.params;

            const result = await productService.getProduct(id);
            
            res.status(200).json({
                data: result,
                message: `${routeName} retrieved`
            });
        } catch (error) {
            next(error);
        }
    });

    router.post("/", 
    passport.authenticate('jwt', {session: false}),
    scopesValidationHandler(['create:products']),
    validationHandler(productCreateSchema), 
    async function(req, res, next){
        try {
            const { body: product } = req;
            const result = await productService.createProduct({product});
            
            res.status(201).json({
                data: result,
                message: `${routeName} created`
            });
        } catch (error) {
            next(error);
        }
    });

    router.put("/:id", 
    passport.authenticate('jwt', {session: false}),
    scopesValidationHandler(['update:products']),
    validationHandler({ id: productIdSchema }, 'params'), 
    validationHandler(prodctUpdateSchema), 
    async function(req, res, next){
        try {
            const { id } = req.params;
            const { body: product} = req;
            
            const result = await productService.updateProduct({id, product});
            
            res.status(200).json({
                data: result,
                message: `${routeName} updated`
            });
        } catch (error) {
            next(error);
        }
    });

    router.delete("/:id", 
    passport.authenticate('jwt', {session: false}),
    scopesValidationHandler(['delete:products']),
    validationHandler({ id: productIdSchema }, 'params'), 
    async function(req, res, next){
        try {
            const { id } = req.params;
            const result = await productService.deleteProduct({id});
            
            res.status(200).json({
                data: result,
                message: `${routeName} deleted`
            });
        } catch (error) {
            next(error);
        }
    });
}

module.exports = productsApi;
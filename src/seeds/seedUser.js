const bcrypt = require('bcrypt');
const chalk = require('chalk');
const debug = require('debug')('app:scripts:users');
const MongoLib = require('./../lib/index');
const { config } = require('./../config/index');

const users = [
  {
    email: 'btaj.code@gmail.com',
    username: 'ROOT',
    password: config.user.defaultAdminPassword,
    isAdmin: true
  },
  {
    email: 'jose@undefined.sh',
    username: 'Jose Maria',
    password: config.user.defaultUserPassword,
    isAdmin: false
  },
  {
    email: 'maria@undefined.sh',
    username: 'Maria Jose',
    password: config.user.defaultUserPassword,
    isAdmin: false
  }
];

async function createUser(mongoDB, user) {
  const { username, email, password, isAdmin } = user;
  const hashedPassword = await bcrypt.hash(password, 10);

  const userId = await mongoDB.create('users', {
    username,
    email,
    password: hashedPassword,
    isAdmin: Boolean(isAdmin)
  });

  return userId;
}

async function seedUsers() {
  try {
    const mongoDB = new MongoLib();

    const promises = users.map(async user => {
      const userId = await createUser(mongoDB, user);
      debug(chalk.green('User created with id:', userId));
    });

    await Promise.all(promises);
    return process.exit(0);
  } catch (error) {
    debug(chalk.red(error));
    process.exit(1);
  }
}

seedUsers();